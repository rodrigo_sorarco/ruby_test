import React, { Component } from "react";
import {
  Alert,
  Container,
  Row,
  Col,
  Form,
  ListGroup,
  Button,
  ButtonToolbar
} from "react-bootstrap";
import { connect } from "react-redux";
import HeaderComponent from "./HeaderComponent";
import {
  create,
  editName,
  editCrmNumber,
  editPhoneNumber,
  editOccupations,
  listOccupations
} from "../store/action/app_action";

class NewComponent extends Component {
  constructor() {
    super();
    this.state = {
      show_alert: false,
      occupation: {},
      occupation_ids: [],
      occupation_selects: []
    };
  }

  componentWillMount() {
    const { listOccupations } = this.props;
    listOccupations();
  }

  _editName = event => {
    const { editName } = this.props;
    editName(event.target.value);
  };

  _editCrmNumber = event => {
    const { editCrmNumber } = this.props;
    editCrmNumber(event.target.value);
  };

  _editPhoneNumber = event => {
    const { editPhoneNumber } = this.props;
    editPhoneNumber(event.target.value);
  };

  _editOccupations = event => {
    const { editOccupations } = this.props;
    editOccupations(event.target.value);
  };

  _selectOccupation = event => {
    this.state.occupation.id = parseInt(
      event.target[event.target.selectedIndex].id
    );
    this.state.occupation.name = event.target.value;
  };

  _addOccupation = () => {
    const { occupation_selects, occupation_ids, occupation } = this.state;
    if (!occupation_selects.find(i => i.id === occupation.id)) {
      occupation_ids.push(occupation.id);
      occupation_selects.push(Object.assign({}, occupation));
    }
    this.setState({
      occupation_selects
    });
  };

  _removeOccupation = id => e => {
    this.setState({
      occupation_ids: this.state.occupation_ids.filter(i => {
        return i != id;
      }),
      occupation_selects: this.state.occupation_selects.filter(i => {
        return i.id != id;
      })
    });
  };

  _create = () => {
    const { occupation_ids } = this.state;
    const { create, messages, name, crm_number, phone_number } = this.props;
    const payload = {
      doctor: {
        name,
        crm_number,
        phone_number,
        occupation_ids
      }
    };
    create(payload);
    this.setState({
      show_alert: !!Object.keys(messages)
    });
  };

  _alertMsg = () => {
    const { messages } = this.props;
    const handleHide = () => this.setState({ show_alert: false });

    return (
      <>
        {this.state.show_alert && (
          <Alert show={this.state.show_alert} variant={messages.type}>
            <Alert.Heading>Messages</Alert.Heading>
            {Object.keys(messages).map(error_key => {
              if (error_key != "type") {
                return (
                  <p key={error_key}>
                    {`${error_key} - ${messages[error_key][0]}`}.
                  </p>
                );
              }
            })}
          </Alert>
        )}
      </>
    );
  };

  _item = item => {
    return (
      <ListGroup.Item key={item.id}>
        {item.name}
        <Button variant="link" onClick={this._removeOccupation(item.id)}>
          remover
        </Button>
      </ListGroup.Item>
    );
  };

  _listItemGroup() {
    const occupations = this.state.occupation_selects;
    const list = occupations.map(this._item);
    return <>{list}</>;
  }

  render() {
    const { occupations } = this.props;
    return (
      <div>
        <HeaderComponent />
        <div style={{ padding: 20 }}>
          <Container>
            <Row>
              <Col>
                <Form>
                  {this._alertMsg()}
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label>Name</Form.Label>
                      <Form.Control
                        type="text"
                        onChange={this._editName}
                        placeholder="Enter name"
                      />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label>CRM number</Form.Label>
                      <Form.Control
                        type="text"
                        onChange={this._editCrmNumber}
                        placeholder="Enter crm number"
                      />
                    </Form.Group>
                  </Form.Row>

                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridCity">
                      <Form.Label>Phone number</Form.Label>
                      <Form.Control
                        type="text"
                        onChange={this._editPhoneNumber}
                        placeholder="Enter phone number"
                      />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                      <Form.Label>Occupations </Form.Label>
                      <Form.Control
                        as="select"
                        onChange={this._selectOccupation}
                      >
                        <option>Choose...</option>
                        {occupations.map(occupation => {
                          return (
                            <option
                              id={occupation.id}
                              value={occupation.name}
                              key={occupation.id}
                            >
                              {occupation.name}
                            </option>
                          );
                        })}
                      </Form.Control>
                      <Button variant="link" onClick={this._addOccupation}>
                        add
                      </Button>
                      <ListGroup style={{ padding: 10 }}>
                        {this._listItemGroup()}
                      </ListGroup>
                    </Form.Group>
                  </Form.Row>

                  <ButtonToolbar>
                    <div style={{ padding: 10 }}>
                      <Button variant="danger" href="/">
                        Cancelar
                      </Button>
                    </div>
                    <div style={{ padding: 10 }}>
                      <Button variant="primary" onClick={this._create}>
                        Create
                      </Button>
                    </div>
                  </ButtonToolbar>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  name: state.AppReducer.name,
  crm_number: state.AppReducer.crm_number,
  phone_number: state.AppReducer.phone_number,
  occupations: state.AppReducer.listOccupations,
  messages: state.AppReducer.messages
});
export default connect(
  mapStateToProps,
  {
    create,
    editName,
    editCrmNumber,
    editPhoneNumber,
    editOccupations,
    listOccupations
  }
)(NewComponent);
