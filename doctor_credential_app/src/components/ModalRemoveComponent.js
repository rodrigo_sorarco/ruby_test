import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";

class ModalRemoveComponent extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false
    };
  }

  handleClose() {
    this.props.handler(this.props.id);
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <>
        <Button variant="link" onClick={this.handleShow}>
          remove
        </Button>

        <Modal show={this.state.show}>
          <Modal.Header closeButton>
            <Modal.Title>Remove record</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure you want remove?</Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleClose}>
              Confirm
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalRemoveComponent;
