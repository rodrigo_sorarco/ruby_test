import React, { Component } from "react";
import {
  Table,
  Button,
  Form,
  FormControl,
  Container,
  Row,
  Col
} from "react-bootstrap";
import ModalRemoveComponent from "./ModalRemoveComponent";
import { connect } from "react-redux";
import {
  searchBy,
  editSearch,
  listDoctors,
  edit,
  remove
} from "../store/action/app_action";

class DoctorList extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    const { listDoctors } = this.props;
    listDoctors();
  }

  _editSearch = event => {
    const { editSearch } = this.props;
    editSearch(event.target.value);
  };

  _edit = id => e => {
    const { edit } = this.props;
    edit(id);
  };

  _remove = id => {
    const { remove } = this.props;
    remove(id);
  };

  _search = () => {
    const { search } = this.props;
    searchBy({ search });
  };

  render() {
    const { list } = this.props;
    return (
      <>
        <Container style={{ padding: 10 }}>
          <Row>
            <Col sm={8}>
              <Form>
                <FormControl
                  type="text"
                  placeholder="Search for name or crm number"
                  onChange={this._editSearch}
                />
              </Form>
            </Col>
            <Col sm={4}>
              <Button variant="outline-success" onClick={this._search}>
                Search
              </Button>
            </Col>
          </Row>
        </Container>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Doctor name</th>
              <th>CRM number</th>
              <th>Phone number</th>
              <th>Occupations</th>
              <th />
              <th />
            </tr>
          </thead>
          <tbody>
            {list.map(doctor => {
              return (
                <tr key={doctor.id}>
                  <td>{doctor.id}</td>
                  <td>{doctor.name}</td>
                  <td>{doctor.crm_number}</td>
                  <td>{doctor.phone_number}</td>
                  <td>
                    {doctor.occupations.map(occupation => {
                      return <div key={occupation.id}>{occupation.name}</div>;
                    })}
                  </td>
                  <td>
                    <Button variant="link" onClick={this._edit(doctor.id)}>
                      edit
                    </Button>
                  </td>
                  <td>
                    <ModalRemoveComponent
                      id={doctor.id}
                      handler={this._remove}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </>
    );
  }
}

const mapStateToProps = state => ({
  list: state.AppReducer.listDoctors,
  search: state.AppReducer.search
});
export default connect(
  mapStateToProps,
  { searchBy, editSearch, listDoctors, edit, remove }
)(DoctorList);
