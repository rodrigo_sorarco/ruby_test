import React, { Component } from "react";
import { Navbar } from "react-bootstrap";
import { connect } from "react-redux";

class HomeComponent extends Component {
  constructor() {
    super();
  }
  componentDidMount() {}

  render() {
    return (
      <div>
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossOrigin="anonymous"
        />
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/">Doctor App</Navbar.Brand>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => ({});
export default connect(
  mapStateToProps,
  {}
)(HomeComponent);
