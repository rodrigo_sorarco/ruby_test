import React, { Component } from "react";
import { connect } from "react-redux";
import app_routes from "../routes/app_routes";
import { Container, Row, Col, ButtonToolbar, Button } from "react-bootstrap";
import HeaderComponent from "./HeaderComponent";
import DoctorList from "./ListComponent";

class HomeComponent extends Component {
  constructor() {
    super();
  }
  componentDidMount() {}

  render() {
    return (
      <div>
        <HeaderComponent />
        <div style={{ padding: 20 }}>
          <Container>
            <Row>
              <Col>
                <DoctorList />
                <ButtonToolbar>
                  <Button variant="link" href={app_routes.new}>
                    New Doctor
                  </Button>
                </ButtonToolbar>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});
export default connect(
  mapStateToProps,
  {}
)(HomeComponent);
