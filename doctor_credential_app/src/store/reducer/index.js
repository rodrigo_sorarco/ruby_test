import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import AppReducer from "./app_reducer";
// import LoginReducer from './login_reducer';
// import FiltroReducer from './filtro_reducer';
// import FiltroPlanoReducer from './filtro_plano_reducer';
// import ModalUsuarioReducer from './modal_usuario_reducer';
// import ModalHistoricoUsuarioReducer from './modal_historico_usuario_reducer';
// import PaginateReducer from './paginate_reducer';

export default history =>
	combineReducers({
		router: connectRouter(history),
		AppReducer
	});
