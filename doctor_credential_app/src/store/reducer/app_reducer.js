import {
  LIST_DOCTORS,
  LIST_OCCUPATIONS,
  OCCUPATIONS_BY_DOCTOR,
  EDIT_SEARCH,
  EDIT_ID,
  EDIT_NAME,
  EDIT_CRM_NUMBER,
  EDIT_PHONE_NUMBER,
  EDIT_OCCUPATIONS,
  MESSAGES
} from "../action/type";

const INITIAL_STATE = {
  occupations_by_doctor: [],
  listDoctors: [],
  listOccupations: [],
  name: "",
  search: "",
  crm_number: "",
  phone_number: "",
  occupations: "",
  create_errors: {},
  id: -1,
  messages: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LIST_DOCTORS:
      return { ...state, listDoctors: action.payload };
    case LIST_OCCUPATIONS:
      return { ...state, listOccupations: action.payload };
    case OCCUPATIONS_BY_DOCTOR:
      return { ...state, occupations_by_doctor: action.payload };
    case EDIT_NAME:
      return { ...state, name: action.payload };
    case EDIT_CRM_NUMBER:
      return { ...state, crm_number: action.payload };
    case EDIT_PHONE_NUMBER:
      return { ...state, phone_number: action.payload };
    case EDIT_OCCUPATIONS:
      return { ...state, occupations: action.payload };
    case EDIT_ID:
      return { ...state, id: action.payload };
    case EDIT_SEARCH:
      return { ...state, search: action.payload };
    case MESSAGES:
      return { ...state, messages: action.payload };
    default:
      return state;
  }
};
