import { applyMiddleware, compose, createStore } from "redux";
import { routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";

import history from "../routes/history";
import reducer from "./reducer";

const middlewares = [routerMiddleware(history), thunk];

const store = createStore(
	reducer(history),
	{},
	compose(applyMiddleware(...middlewares))
);

export default store;
