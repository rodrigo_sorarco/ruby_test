import axios from "axios";
import { push } from "connected-react-router";
import app_routes from "../../routes/app_routes";
import {
  MESSAGES,
  LIST_DOCTORS,
  LIST_OCCUPATIONS,
  OCCUPATIONS_BY_DOCTOR,
  EDIT_ID,
  EDIT_SEARCH,
  EDIT_NAME,
  EDIT_CRM_NUMBER,
  EDIT_PHONE_NUMBER,
  EDIT_OCCUPATIONS
} from "./type";

export const editSearch = search => {
  return {
    type: EDIT_SEARCH,
    payload: search
  };
};

export const editName = edit_name => {
  return {
    type: EDIT_NAME,
    payload: edit_name
  };
};

export const editCrmNumber = edit_crm_number => {
  return {
    type: EDIT_CRM_NUMBER,
    payload: edit_crm_number
  };
};

export const editPhoneNumber = edit_phone_number => {
  return {
    type: EDIT_PHONE_NUMBER,
    payload: edit_phone_number
  };
};

export const editOccupations = edit_occupations => {
  return {
    type: EDIT_OCCUPATIONS,
    payload: edit_occupations
  };
};

export const searchBy = query => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.doctors}?q=${query}`;
      const response = await axios.get(rota, {
        "Content-Type": "application/json"
      });
      dispatch({
        type: LIST_DOCTORS,
        payload: response.data
      });
      dispatch(push(app_routes.home));
    } catch (error) {}
  };
};

export const listDoctors = () => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.doctors}`;
      const response = await axios.get(rota, {
        "Content-Type": "application/json"
      });
      dispatch({
        type: LIST_DOCTORS,
        payload: response.data
      });
      dispatch(push(app_routes.home));
    } catch (error) {}
  };
};

export const listOccupations = () => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.occupations}`;
      const response = await axios.get(rota, {
        "Content-Type": "application/json"
      });
      dispatch({
        type: LIST_OCCUPATIONS,
        payload: response.data
      });
    } catch (error) {}
  };
};

export const edit = id => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.doctors}/${id}`;
      const response = await axios.get(rota, {
        "Content-Type": "application/json"
      });

      dispatch({
        type: EDIT_ID,
        payload: id
      });
      dispatch({
        type: EDIT_NAME,
        payload: response.data.name
      });
      dispatch({
        type: EDIT_CRM_NUMBER,
        payload: response.data.crm_number
      });
      dispatch({
        type: EDIT_PHONE_NUMBER,
        payload: response.data.phone_number
      });
      dispatch({
        type: OCCUPATIONS_BY_DOCTOR,
        payload: response.data.occupations
      });
      dispatch(push(app_routes.edit));
    } catch (error) {}
  };
};

export const create = payload => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.doctors}`;

      const headers = {
        "Content-Type": "application/json"
      };

      await axios.post(rota, payload, { headers });

      dispatch({
        type: MESSAGES,
        payload: {
          msg: ["New Doctor record created successfully"],
          type: "success"
        }
      });
      dispatch(push(app_routes.home));
    } catch (error) {
      const _error = error.response.data;
      _error.type = "danger";
      dispatch({
        type: MESSAGES,
        payload: _error
      });
    }
  };
};

export const update = (payload, id) => {
  return async dispatch => {
    try {
      const rota = `${app_routes.host}/${app_routes.doctors}/${id}`;

      const headers = {
        "Content-Type": "application/json"
      };

      await axios.put(rota, payload, { headers });

      dispatch(push(app_routes.home));
    } catch (error) {
      const _error = error.response.data;
      _error.type = "danger";
      dispatch({
        type: MESSAGES,
        payload: _error
      });
    }
  };
};

export const remove = id => {
  return async dispatch => {
    try {
      let rota = `${app_routes.host}/${app_routes.doctors}/${id}`;
      await axios.delete(rota, {
        "Content-Type": "application/json"
      });

      rota = `${app_routes.host}/${app_routes.doctors}`;
      const response = await axios.get(rota, {
        "Content-Type": "application/json"
      });
      dispatch({
        type: LIST_DOCTORS,
        payload: response.data
      });
      dispatch(push(app_routes.home));
    } catch (error) {
      console.log(error);
    }
  };
};
