export default {
  host: "http://localhost:3001/api/v1",
  doctors: "doctors",
  occupations: "occupations",
  home: "/",
  new: "/new",
  edit: "/edit"
};
