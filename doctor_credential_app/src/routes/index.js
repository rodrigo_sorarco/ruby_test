import React from "react";
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import history from "./history";
import app_routes from "./app_routes";
// import '../assets/css/style.min.css';
// import '../assets/fonts//material-design-iconic-font/css/material-design-iconic-font.min.css';
// import '../assets/fonts/dripicons/css/dripicons.css';

// import rotas from '../components/rotas';
import HomeComponent from "../components/HomeComponent";
import NewComponent from "../components/NewComponent";
import EditComponent from "../components/EditComponent";
// import DoctorList from "../components/DoctorList";
// import Main from '../components/Main';
// import PainelAdminUsuarioComponent from '../components/PainelAdminUsuarioComponent';
// import PainelAdminPlanoComponent from '../components/PainelAdminPlanoComponent';
// import LoginComponent from '../components/LoginComponent';
// import Profile from '../components/Profile';
// import Planos from '../components/Planos';

const Routes = () => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact path={app_routes.home} component={HomeComponent} />
      <Route exact path={app_routes.new} component={NewComponent} />
      <Route exact path={app_routes.edit} component={EditComponent} />
    </Switch>
  </ConnectedRouter>
);

export default Routes;
