### doctor_credential_app

\*\* App (react/redux)

### doctor_credential_api

\*\* Rails Api

### run app (api and app)

$ cd doctor_credential_api
$ bundle install
$ rails db:drop db:create db:migrate db:seed 
$ rails s -p 3001

$ cd doctor_credential_app
$ yarn install

### Test Api

$ cd doctor_credential_api 
$ bundle exec rspec spec

### Setup and run - docker

$ docker-compose run frontend yarn 
$ docker-compose run backend rake db:drop db:create db:migrate db:seed
$ docker-compose up -d $ open [http://localhost:3000](http://localhost:3000/)
