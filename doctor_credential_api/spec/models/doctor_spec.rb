require 'rails_helper'

RSpec.describe Doctor, type: :model do
  it "is invalid if the occupations is the minimum allowed" do
    occupations = [
      build(:occupation,  name:"ALERGOLOGIA", description:"especialidade de atuação alergologia" ),
      build(:occupation, name:"BUCO MAXILO", description:"especialidade de atuação buco maxilo" )
    ]
    expect(build(:doctor, occupations: occupations)).to be_valid 
  end
end
