require "rails_helper"

RSpec.describe "Doctor management", :type => :request do

  before do
    occupation_1 = create(:occupation,  name:"ALERGOLOGIA", description:"especialidade de atuação alergologia" )
    occupation_2 = create(:occupation,  name:"ALERGOLOGIA", description:"especialidade de atuação alergologia" )

    occupations = [
        occupation_1,
        occupation_2
    ]

    @doctor = create(:doctor, name:"Carlos",crm_number:123456,phone_number:"(11) 99923-1687", occupations: occupations)

  end

  it "GET /api/v1/doctors" do
    get "/api/v1/doctors"
    expect(response).to  have_http_status(200)
  end

  it "POST /api/v1/doctors" do
    doctor_attributes = {name:"Carlos",crm_number:123456,phone_number:"(11) 99923-1687", occupation_ids: [1,2]}
    post "/api/v1/doctors", params: {doctor: doctor_attributes}
    expect(Doctor.last).to have_attributes(doctor_attributes)
  end

  it "GET /api/v1/doctors?q=Carlos" do
    get "/api/v1/doctors", params: {q:"Carlos"}
    expect(response).to have_http_status(200)
  end

  it "GET /api/v1/doctors/:id" do
    get "/api/v1/doctors/#{@doctor.id}"
    expect(response).to have_http_status(200)
  end

  it "PUT /api/v1/doctors/:id" do
    doctor_attributes = {name:"Carlos Silva",crm_number:123456,phone_number:"(11) 99923-1687", occupation_ids: [1,2]}
    put  "/api/v1/doctors/#{@doctor.id}", params: {doctor: doctor_attributes}
    expect(Doctor.last).to have_attributes(doctor_attributes)
  end

  it "DELETE /api/v1/doctors/:id" do
    delete "/api/v1/doctors/#{@doctor.id}"
    expect(response).to have_http_status(204)
  end

end