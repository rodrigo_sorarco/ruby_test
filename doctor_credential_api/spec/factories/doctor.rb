FactoryBot.define do
    factory :doctor do
      name {"John"}
      crm_number  {12345}
      phone_number {"(11) 99923-1687"}
    end
  end