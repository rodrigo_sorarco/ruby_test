module Api::V1
    class DoctorsController < ApiController
        before_action :set_occupation, only: [:create, :update]
        def index
            filter = params[:q]
            render json: Doctor.search(filter).order('created_at DESC')
        end

        def show
            doctor = Doctor.find(params[:id])
            render json: doctor, status: :ok
        end

        def create
            doctor = Doctor.new(doctor_params)
            doctor.occupations << @occupation
            if doctor.save
              render json: doctor, status: :created
            else
              render json: doctor.errors, status: :unprocessable_entity
            end
        end

        def update
            doctor = Doctor.find(params[:id])
            doctor.occupations = @occupation
            if doctor.update(doctor_params)
                render json: doctor,status: :ok
            else
                render json: doctor.errors, status: :unprocessable_entity
            end

        end

        def destroy
            doctor = Doctor.find(params[:id])
            if doctor.destroy
                head :no_content, status: :ok
            else
                render json: @list.errors, status: :unprocessable_entity
            end  
        end

        private
        def set_occupation
            @occupation = Occupation.find(params[:doctor][:occupation_ids])
        end


        # Only allow a trusted parameter.
        def doctor_params
            params.require(:doctor).permit(:name, :crm_number, :phone_number, :occupation_ids)
        end
    end
end
