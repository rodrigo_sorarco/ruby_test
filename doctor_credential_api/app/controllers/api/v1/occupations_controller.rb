module Api::V1
    class OccupationsController < Api::V1::ApiController
        def index
            render json: Occupation.all.order('name ASC')
        end
    end
end
