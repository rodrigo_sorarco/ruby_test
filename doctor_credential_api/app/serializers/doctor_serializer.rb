class DoctorSerializer < ActiveModel::Serializer
  attributes :id, :name, :crm_number, :phone_number, :occupations
end
