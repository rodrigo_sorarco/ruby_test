class OccupationSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
