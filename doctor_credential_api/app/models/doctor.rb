class Doctor < ApplicationRecord
  
    has_many :doctorOccupations
    has_many :occupations, through: :doctorOccupations
    accepts_nested_attributes_for :occupations, allow_destroy: true

    validates :name, :crm_number, :phone_number,  presence: true
    validates :crm_number, numericality: { only_integer: true }
    validates :occupations, length: { minimum: 2, too_short: "%{count} occupations is the minimum allowed" }
    
    def self.search(search)
      if search
        doctor = Doctor.where("name LIKE ? OR crm_number = ?", "%#{search}%", search)
      else
        Doctor.all
      end
    end

    def occupations_validation
        if occupations.size < 2
          errors.add(:occupations, "can't be less than two value or with same value")
        end
    end
end
