class DoctorOccupation < ApplicationRecord
  belongs_to :doctor
  belongs_to :occupation
end
