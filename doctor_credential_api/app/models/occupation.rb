class Occupation < ApplicationRecord
    has_many :doctorOccupations
    has_many :doctors, through: :doctorOccupations
end
