class CreateDoctors < ActiveRecord::Migration[5.2]
  def change
    create_table :doctors do |t|
      t.string :name
      t.integer :crm_number
      t.string :phone_number

      t.timestamps
    end
  end
end
