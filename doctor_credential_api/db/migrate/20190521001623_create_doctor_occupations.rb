class CreateDoctorOccupations < ActiveRecord::Migration[5.2]
  def change
    create_table :doctor_occupations do |t|
      t.references :doctor, foreign_key: true
      t.references :occupation, foreign_key: true

      t.timestamps
    end
  end
end
